import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Role } from 'src/roles/entities/role.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Role) private rolesRepository: Repository<Role>,
  ) { }
  create(createUserDto: CreateUserDto) {
    const newUser = new User();
    newUser.email = createUserDto.email
    newUser.fullName = createUserDto.fullName
    newUser.gender = createUserDto.gender
    newUser.password = createUserDto.password
    newUser.roles = JSON.parse(createUserDto.roles)
    if (createUserDto.image && createUserDto.image !== '') {
      newUser.image = createUserDto.image
    }

    console.log(newUser)
    return this.usersRepository.save(newUser);
  }


  findAll() {
    return this.usersRepository.find({
      relations: { roles: true }
    });
  }

  findOne(id: number) {
    return this.usersRepository.findOne({
      where: { id },
      relations: { roles: true },
    });
  }

  findOneByEmail(email: string) {
    return this.usersRepository.findOneByOrFail({ email });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = new User();
    user.email = updateUserDto.email
    user.fullName = updateUserDto.fullName
    user.gender = updateUserDto.gender
    user.password = updateUserDto.password
    user.roles = JSON.parse(updateUserDto.roles)
    if (updateUserDto.image && updateUserDto.image !== '') {
      user.image = updateUserDto.image
    }
    const updateUser = await this.usersRepository.findOneOrFail({
      where: { id },
      relations: { roles: true }
    });
    updateUser.email = user.email;
    updateUser.fullName = user.fullName;
    updateUser.gender = user.gender;
    updateUser.password = user.password;
    updateUser.image = user.image
    for (const r of user.roles) {
      const searhRole = updateUser.roles.find((role) => role.id === r.id)
      if (!searhRole) {
        const newRole = await this.rolesRepository.findOneBy({ id: r.id });
        updateUser.roles.push(newRole);
      }
    }
    for (let i = 0; i < updateUser.roles.length; i++) {
      const index = user.roles.findIndex((role) => role.id === updateUser.roles[i].id)
      if (index < 0) {
        updateUser.roles.splice(index, 1);
      }
    }
    await this.usersRepository.save(updateUser)
    const result = await this.usersRepository.findOne({
      where: { id },
      relations: { roles: true },
    });
    return result;
  }

  async remove(id: number) {
    const deleteUser = await this.usersRepository.findOneOrFail({
      where: { id },
    });
    await this.usersRepository.remove(deleteUser);

    return deleteUser;
  }
}
